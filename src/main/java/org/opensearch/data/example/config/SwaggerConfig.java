package org.opensearch.data.example.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    @Bean
    public OpenAPI apiInfo() {
        return new OpenAPI().info(new Info().title("OpenSearch WiKi example")
                .description("OpenSearch & Tika prototype ")
                .version("v0.0.1")
                .contact(getContactDetails()));
    }

    private Contact getContactDetails() {
        return new Contact().name("Vladimir Saenko")
                .email("v.saenko@bftcom.com")
                .url("https://bftcom.com/");
    }
}