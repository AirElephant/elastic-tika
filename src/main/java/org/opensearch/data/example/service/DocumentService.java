package org.opensearch.data.example.service;

import org.opensearch.data.example.model.WikiItemDTO;
import org.opensearch.data.example.repository.WikiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DocumentService {
    @Autowired
    private WikiRepository wikiRepository;

    public Iterable<WikiItemDTO> getDocuments() {
        return wikiRepository.findAll();
    }

    public WikiItemDTO insertDocument(WikiItemDTO document) {
        return wikiRepository.save(document);
    }

    public WikiItemDTO updateDocument(WikiItemDTO document, String id) {
        WikiItemDTO document1 = wikiRepository.findById(id).get();
        document1.setBody(document.getBody());
        return document1;
    }
    public void deleteById(String id){
        wikiRepository.deleteById(id);
    }
}
