package org.opensearch.data.example.service;

import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class DocumentParserService {

    public String parseDocument(MultipartFile file) throws IOException, TikaException {
        Tika tika = new Tika();
        return tika.parseToString(file.getInputStream());
    }

    public boolean searchInDocument(String textToSearch, MultipartFile file) throws IOException, TikaException {
        String parsedText = parseDocument(file);
        return parsedText.contains(textToSearch);
    }


}
