package org.opensearch.data.example.repository;


import org.opensearch.data.example.model.WikiItemDTO;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WikiRepository extends ElasticsearchRepository<WikiItemDTO, String> {

}