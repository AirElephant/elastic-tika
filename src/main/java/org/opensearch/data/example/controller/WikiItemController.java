package org.opensearch.data.example.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.tika.exception.TikaException;
import org.opensearch.data.example.model.WikiItemDTO;
import org.opensearch.data.example.service.DocumentParserService;
import org.opensearch.data.example.service.DocumentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/v1/documents")
@Tag(name= "Parsing documents",description = "Парсим документ")
public class WikiItemController {

    private final DocumentParserService documentParserService;
    private final DocumentService documentService;

    public WikiItemController(DocumentParserService documentParserService, DocumentService documentService) {
        this.documentParserService = documentParserService;
        this.documentService = documentService;
    }


    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/parse", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "Получаем текст документа")
    public ResponseEntity<String> parseDocument(@RequestParam("file") MultipartFile file) {
        try {
            String parsedText = documentParserService.parseDocument(file);
            return ResponseEntity.ok(parsedText);
        } catch (IOException | TikaException e) {
            return ResponseEntity.status(500).body("Ошибка при парсинге документа: " + e.getMessage());
        }
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/search", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "Ищем в тексте документа")
    public ResponseEntity<String> searchInDocument(
            @RequestParam("file") MultipartFile file,
            @RequestParam("textToSearch") String textToSearch) {
        try {
            boolean found = documentParserService.searchInDocument(textToSearch, file);
            if (found) {
                return ResponseEntity.ok("Текст " + "`" + textToSearch + "`" + " найден.");
            } else {
                return ResponseEntity.ok("Текст не найден в документе.");
            }
        } catch (IOException e) {
            return ResponseEntity.status(500).body("Ошибка при поиске в документе: " + e.getMessage());
        } catch (TikaException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/findAll")
    Iterable<WikiItemDTO> findAll() {
        return documentService.getDocuments();
    }

    @PostMapping("/insert")
    public WikiItemDTO insertDocument(@RequestBody WikiItemDTO document) {
        return documentService.insertDocument(document);
    }

    @DeleteMapping("/{id}")
    public void deleteDocument(@PathVariable String id){
        documentService.deleteById(id);
    }
}
