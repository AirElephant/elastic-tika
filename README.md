# ElasticSearch Example with Spring Boot and Tika

## Introduction

This example demonstrates how to use Spring Data ElasticSearch with Tika

For this example, we created a Controller that allows doing the following operations with OpenSearch:

- Parse file
- Find word in parsed file

## How to run

The first thing to do is to start ElasticSearch. 
(you can download it from here: [https://www.elastic.co/downloads/elasticsearch](https://www.elastic.co/downloads/elasticsearch))

Then you can run the application.

Once everything is up and running open the browser and go to [http://localhost:8080](http://localhost:8080). You should
see Swagger to interact with.

you can also follow the link [https://localhost:9200](https://localhost:9200) and play with elastic

